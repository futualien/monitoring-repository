<?php

return [
    'routes' => [
        'prefix' => 'api/v1/',
        'middleware' => 'auth:api',
    ],
    'filesystem' => [
        'disk' => 'local',
        'path' => null
    ]
];