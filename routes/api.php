<?php
Route::group([
    'prefix' => config('ledsi.repository.routes.prefix'),
    'namespace' => 'LEDsi\Repository\Controllers'
], static function () {
    Route::group([
        'middleware' => config('ledsi.repository.routes.middleware', ['auth:api'])
    ], static function () {
        Route::resource('repository', 'RepositoryController')->only(['index', 'store', 'show', 'update', 'destroy']);
    });
});
