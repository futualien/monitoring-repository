<?php

namespace LEDsi\Repository;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/repository.php', 'ledsi.repository');

        $this->loadRoutesFrom(__DIR__ . '/../routes/api.php');

        //$this->loadMigrationsFrom(__DIR__ . '/../database/migrations/');
    }

    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../config/repository.php' => config_path('ledsi/repository.php'),
        ], 'config');

        $this->publishes([
            __DIR__ . '/../database/migrations/' => database_path('migrations/'),
        ], 'migrations');

        $this->publishes([
            __DIR__ . '/Models/Repository.php' => app_path('Models/Repository.php'),
            __DIR__ . '/Services/RepositoryService.php' => app_path('Services/RepositoryService.php'),
            __DIR__ . '/Controllers/RepositoryController.php' => app_path('Http/Contollers/RepositoryController.php'),
        ], 'resources');
    }
}
