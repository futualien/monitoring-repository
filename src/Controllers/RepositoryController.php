<?php

namespace LEDsi\Repository\Controllers;

use App\Http\Controllers\Controller;
use LEDsi\Repository\Services\RepositoryServiceInterface;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class RepositoryController extends Controller
{
    protected $service;

    public function __construct(RepositoryServiceInterface $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        return response()->json([
            'list' => $this->service->index()
        ]);
    }

    public function store(Request $request)
    {
        $validated = $request->validate($this->service->rules('create'));
        try {
            $data = $this->service->create($validated, $request->allFiles());
        } catch (Exception $exception) {
            return response()->json(['error' => $exception->getMessage()], 500);
        }
        return response()->json($data, 201);
    }

    public function show(Request $request, $key)
    {
        try {
            $data = $this->service->show($key);
        } catch (ModelNotFoundException $exception) {
            return response()->json(['error' => $exception->getMessage()], 404);
        }
        return response()->json($data);
    }

    public function update(Request $request, $key)
    {
        $validated = $request->validate($this->service->rules('update'));
        try {
            $data = $this->service->update($key, $validated, $request->allFiles());
        } catch (ModelNotFoundException $exception) {
            return response()->json(['error' => $exception->getMessage()], 404);
        } catch (\Exception $exception) {
            return response()->json(['error' => $exception->getMessage()], 500);
        }
        return response()->json($data);
    }

    public function destroy($key)
    {
        try {
            $deleted = $this->service->delete($key);
        } catch (\Exception $exception) {
            return response()->json(['error' => $exception->getMessage()], 500);
        }
        return response()->json(['deleted' => $deleted], 200);
    }

    public function latest(Request $request) {
        try {
            $latest = $this->service->getLatestVersions($request->all());
        } catch (\Exception $exception) {
            return response()->json(['error' => $exception->getMessage()], 500);
        }
        return response()->json([
            'list' => $latest
        ]);
    }
}
