<?php

namespace LEDsi\Repository\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

abstract class AbstractRepository extends Model
{
    protected $table = 'repository';

    protected $fillable = ['name', 'version', 'src'];

    protected $appends = ['url'];

    public function getRules(string $action = null): array
    {
        $rules = [
            'create' => [
                'name' => ['required', 'string', 'max:255'],
                'version' => ['required', 'string', 'max:255'],
                'src' => ['required', 'file', 'max:102400']
            ],
            'update' => [
                'name' => ['sometimes', 'string', 'max:255'],
                'version' => ['sometimes', 'string', 'max:255'],
                'src' => ['sometimes', 'file', 'max:102400']
            ]
        ];
        return $action ? ($rules[$action] ?? []) : $rules;
    }

    protected static function boot()
    {
        parent::boot();

        self::saved(function ($model) {
            if ($model->is_latest && $model->getDirty('is_latest')) {
                self::name($model->name)->ignore($model->id)->update(['is_latest' => false]);
            }
        });
    }


    public function setSrcAttribute($value)
    {
        $this->attributes['src'] = $value;
        $this->hash = $this->generateHash();
    }

    public function setVersionAttribute($value)
    {
        $this->attributes['version'] = $value;
        $this->version_number = $this->generateVersionNumber();
        $this->is_latest = $this->checkIsLatestVersion();
    }

    public function getUrlAttribute()
    {
        $disk = config('ledsi.repository.filesystem.disk', config('filesystems.default', 'local'));
        return Storage::disk($disk)->url($this->src);
    }


    public function scopeName($q, string $name)
    {
        return $q->where('name', $name);
    }

    public function scopeVersion($q, string $version)
    {
        return $q->where('version', $version);
    }

    public function scopeHash($q, string $hash)
    {
        return $q->where('hash', $hash);
    }

    public function scopeLatestVersion($q)
    {
        return $q->where('is_latest', true);
    }

    public function scopeIgnore($q, $id)
    {
        return $q->where('id', '!=', $id);
    }

    public function scopeSearch($q, array $filters)
    {
        return $q->when(!empty($filters['name']), function ($q) use ($filters) {
            $q->name($filters['name']);
        })
            ->when(!empty($filters['version']), function ($q) use ($filters) {
                $q->name($filters['version']);
            });
    }


    public function isSame(string $name, string $version, string $hash): bool
    {
        return $this->name === $name
            && $this->version === $version
            && $this->hash === $hash;
    }

    public function isLatestVersion(): bool
    {
        return $this->is_latest;
    }

    public function checkIsLatestVersion(int $version = null): bool
    {
        $maxVersion = self::name($this->name)->max('version_number');
        return ($version ?? $this->version_number) >= $maxVersion;
    }


    public function generateHash(string $path = null): ?string
    {
        $path = $path ?? $this->src;
        $disk = config('ledsi.repository.filesystem.disk', config('filesystems.default', 'local'));
        $exists = Storage::disk($disk)->exists($path);
        $path = Storage::disk($disk)->path($path);
        return $exists ? md5_file($path) : null;
    }

    public function generateVersionNumber(string $version = null): int
    {
        $explodedVersion = explode('.', $version ?? $this->version);
        foreach ($explodedVersion as $key => &$chunk) {
            $chunk *= 100 ^ (count($explodedVersion) - $key - 1);
        };
        return array_sum($explodedVersion);
    }
}
