<?php

namespace LEDsi\Repository\Services;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

abstract class AbstractRepositoryService implements RepositoryServiceInterface
{
    protected $model;

    public function model()
    {
        return $this->model;
    }

    public function rules(string $action = null): array
    {
        return (new $this->model)->getRules($action);
    }

    public function all(): array
    {
        return $this->model::all()->toArray();
    }

    public function index(): array
    {
        return $this->all();
    }

    public function create(array $attributes, array $files): array
    {
        if (!empty($files['src']) && $files['src'] instanceof UploadedFile) {
            $attributes['src'] = $this->saveFile($files['src']);
        }
        $model = $this->model::create($attributes);
        if ($model) {
            static::created($model, $attributes);
        }
        return $model->toArray();
    }

    abstract public static function created(&$model, array $attributes);

    public function show($key): array
    {
        return $this->model::findOrFail($key)->toArray();
    }

    public function update($key, array $attributes, array $files): array
    {
        $model = $this->model::findOrFail($key);
        $oldFilePath = $model->src;
        if (!empty($files['src']) && $files['src'] instanceof UploadedFile) {
            $attributes['src'] = $this->saveFile($files['src']);
        }
        $model->fill($attributes);
        $saved = $model->save();
        if ($saved) {
            if($oldFilePath) {
                $this->deleteFile($oldFilePath);
            }
            static::updated($model, $attributes);
        }
        return $model->toArray();
    }

    abstract public static function updated(&$model, array $attributes);

    public function delete($key): bool
    {
        $model = $this->model::findOrFail($key);
        $deleted = $model->delete();
        if ($deleted) {
            $this->deleteFile($model->src);
            static::deleted($model);
        }
        return $deleted;
    }

    abstract public static function deleted(&$model);

    public function getLatestVersions(array $filters): array
    {
        return $this->model::latestVersion()
            ->search($filters)
            ->get()->toArray();
    }

    protected function saveFile(UploadedFile $file): string
    {
        return $file->store(
            config('ledsi.repository.filesystem.path', null),
            [
                'disk' => config('ledsi.repository.filesystem.disk', config('filesystems.default', 'local'))
            ]
        );
    }

    protected function deleteFile(string $path): bool
    {
        $disk = config('ledsi.repository.filesystem.disk', config('filesystems.default', 'local'));
        if (Storage::disk($disk)->exists($path)) {
            $deleted = Storage::disk($disk)->delete($path);
        }
        return $deleted ?? false;
    }
}
