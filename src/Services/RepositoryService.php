<?php

namespace App\Services;

use App\Models\Repository;
use LEDsi\Repository\Services\AbstractRepositoryService;

class RepositoryService extends AbstractRepositoryService
{
    protected $model = Repository::class;

    public static function created(&$model, array $attributes)
    {
    }

    public static function updated(&$model, array $attributes)
    {
    }

    public static function deleted(&$model)
    {
    }
}
