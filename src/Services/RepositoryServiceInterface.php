<?php

namespace LEDsi\Repository\Services;

interface RepositoryServiceInterface
{
    public function model();

    public function all(): array;

    public function index(): array;

    public function create(array $attributes, array $files): array;

    public static function created(&$model, array $attributes);

    public function show($key): array;

    public function update($key, array $attributes, array $files): array;

    public static function updated(&$model, array $attributes);

    public function delete($key): bool;

    public static function deleted(&$model);

    public function getLatestVersions(array $filters): array;

    public function rules(string $action = null): array;
}
